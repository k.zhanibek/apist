from setuptools import setup, find_packages

setup(name='apist',
      version='0.2',
      url='https://gitlab.com/aqa4774236/apist',
      license='NOLICENCE',
      author='NO AUTHOR',
      description='Framework for automated tests API',
      long_description='',
      zip_safe=False)
