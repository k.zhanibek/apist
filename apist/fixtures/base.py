import json
import logging

import cattr
from requests import Response


class BaseClass:
    def to_dict(self) -> dict:
        """
        Convert nested object to dict
        :return: dict
        """
        return json.loads(json.dumps(self, default=lambda o: o.__dict__))


class Validator:
    @staticmethod
    def structure(response: Response, type_response) -> Response:
        """
        Try to structure response
        :param response: response
        :param type_response: type response
        :return: modify response with "data" field
        """
        if type_response:
            try:
                response.data = cattr.structure(response.json(), type_response)
            except Exception as e:
                logging.info(response.content)
                raise e
        return response
