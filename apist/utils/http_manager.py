from functools import reduce
import os

import allure
import requests
from allure_commons.types import AttachmentType
from assertpy import assert_that, soft_assertions

from apist.utils.constants import RESPONSE_TIME
from apist.utils.qase_utils import qase_request_parameters_text


class HttpManager:

    @staticmethod
    def request(method: str = None, base_url: str = None, raw_path: str = None, uri_params: dict = None,
                url: str = None, **kwargs):
        """
        Request method
        method: method for the new Request object: GET, OPTIONS, HEAD, POST, PUT, PATCH, or DELETE.
        url – URL for the new Request object.
        **kwargs:
            params – (optional) Dictionary, list of tuples or bytes to send in the query string for the Request.
            json – (optional) A JSON serializable Python object to send in the body of the Request.
            headers – (optional) Dictionary of HTTP Headers to send with the Request.
        """


        # if COVERAGE:
        #     __uri = URI(base_url, raw_path, **uri_params)
        #     response = requests.request(method, __uri.full, **kwargs, timeout=(1, 31))
        #     if not IS_DISABLED:
        #         RequestSchemaHandler(__uri, method, response, kwargs).write_schema()
        #
        # elif not COVERAGE:

        response = requests.request(method, url, **kwargs, timeout=(1, 31))
        qase_request_parameters_text(response)

        return response


class HttpTarget:
    def __init__(self, base_path=None):
        self.base_path = base_path

    def base_path(self, base_path):
        self.base_path = base_path
        return self

    def endpoint(self, endpoint):
        _end_point = self.base_path + endpoint
        return _end_point


class HttpAssert(object):

    @staticmethod
    def assert_ok_status_code(response, soft=False):

        if not soft:
            with allure.step('Получить 2** статус код ОК от сервера (Hard Assert)'):
                assert_that(response.status_code, description='Status code не равен успешному!'). \
                    is_equal_to(requests.codes.ok)

        elif soft:
            with allure.step('Получить необязательный 2** статус код от сервера (Soft Assert)'):
                with soft_assertions():
                    assert_that(response.status_code, description='Status code не равен успешному!'). \
                        is_equal_to(requests.codes.ok)

    @staticmethod
    def assert_specific_status_code(response, code, soft=False):
        if not soft:
            with allure.step(f'Получить {str(code)} статус код от сервера (Hard Assert)'):
                assert_that(response.status_code, description='Status code не равен  ожидаемому!').is_equal_to(code)

        elif soft:
            with allure.step(f'Получить необязательный {str(code)} статус код от сервера (Soft Assert)'):
                with soft_assertions():
                    assert_that(response.status_code, description='Status code не равен успешному!'). \
                        is_equal_to(code)

    @staticmethod
    def assert_failed_status_code(response):
        assert_that(response.status_code, description='Status code не равен успешному!').is_equal_to(
            requests.codes.failed)

    @staticmethod
    def assert_in_response_body(response, *keys, value):
        value_from_key = reduce(lambda d, key: d.get(key) if d else None, keys, response.json())

        assert_that(value_from_key, description=f'JSON key: {str(keys[-1])} is not contains'
                                                f' to expected value:{str(value)} '
                                                f'real value:{value_from_key}').is_equal_to(value)

    @staticmethod
    def assert_not_equal(response, key, value):
        assert_that(response.json()[key], description=f'JSON key: {str(key)} is equal to not expected value'
                                                      f'not expected value:{str(value)}  '
                                                      f'value:{response.json()[key]}').is_not_equal_to(value)

    @staticmethod
    def assert_equal(value_from_server, expected_value, equal=True):
        if equal:
            assert_that(value_from_server).is_equal_to(expected_value)
        if not equal:
            assert_that(value_from_server).is_not_equal_to(expected_value)

    @staticmethod
    def assert_multiple_value(value_from_server, expected_value, key):
        list_of_index = list(range(len(expected_value)))
        for x, y in zip(list_of_index, expected_value):
            assert_that(value_from_server[x][key]).is_equal_to(y)

    @staticmethod
    def assert_type(value_from_server, type):
        assert_that(value_from_server).is_type_of(type)

    @staticmethod
    def assert_greater(value_from_server, expected_value):
        assert_that(value_from_server).is_greater_than_or_equal_to(expected_value)

    @staticmethod
    def assert_by_key(dict_value, key, assert_value):
        assert_that(dict_value[key]).is_equal_to(assert_value)

    @staticmethod
    def assert_response_symbol_sym_more_than(response, value):
        assert len(response.text) > value

    @staticmethod
    def assert_response_body_json(response, json):
        assert_that(response.json()).is_equal_to(json)

    @staticmethod
    def assert_response_time_not_more_than(response, time):
        with allure.step(f'Проверить то что время ожидания ответа от сервера '
                         f'не больше: {str(RESPONSE_TIME)} '):
            try:
                assert_that(response.elapsed.total_seconds()).is_less_than_or_equal_to(time)
                allure.attach(f'✅ Успешно, время ответа: {str(response.elapsed.total_seconds())}',
                              attachment_type=AttachmentType.TEXT, name='Время ответа от сервера')

            except AssertionError:
                allure.attach('❌Если вы видите это сообщение то тест упал из-за того что время ответа от сервера'
                              f'составило больше: {str(time)} сек. Фактическое время ответа:'
                              f'{str(response.elapsed.total_seconds())}',
                              attachment_type=AttachmentType.TEXT, name='Ошибка ответа от сервера')

    @staticmethod
    def assert_contains(response, expected_value):
        assert_that(response).contains(expected_value)

    @staticmethod
    def assert_body_empty(response):
        with allure.step("Проверить что тело ответа пустое"):
            assert_that(response.content).is_empty()

        if not response.content:
            allure.attach(response.content, name="Тело ответа не пустое!", attachment_type=AttachmentType.TEXT)


class HttpParse:

    @staticmethod
    def return_response_json(response):
        json = response.json()
        return json

    @staticmethod
    def return_by_key(response, *keys):
        value_by_key = reduce(lambda d, key: d.get(key) if d else None, keys, response.json())
        return value_by_key

