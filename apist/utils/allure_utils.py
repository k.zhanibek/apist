import json

import allure
import curlify
from allure_commons.types import AttachmentType


def return_encode_json_response(res):
    user_encode_data = json.dumps(res.json(), indent=1, ensure_ascii=False).encode('utf-8')

    return user_encode_data


def return_encode_json_request(res):
    user_encode_data = json.dumps(res.request.body, indent=1, ensure_ascii=False).encode('utf-8')

    return user_encode_data


def allure_request_body(request):
    decoded_dict = json.loads(request.request.body.decode('utf-8'))
    allure.attach(json.dumps(decoded_dict, indent=1, ensure_ascii=False).encode('utf-8'),
                  attachment_type=AttachmentType.JSON, name='Тело запроса JSON')


def allure_response_body(request):
    allure.attach(return_encode_json_response(request), attachment_type=AttachmentType.JSON, name='Тело ответа JSON')


def allure_response_body_not_json(request):
    allure.attach(request.text(), attachment_type=AttachmentType.TEXT, name='Тело ответа')


def allure_request_parameters_text(request):
    title = '==================REQUEST=INFO=================='
    request_method = f"Request method: {request.request.method}"
    request_url = f"Request URL: {request.url}"
    request_head = f'Request head: {request.request.headers}'
    curl = '==================REQUEST=CURL=================='
    curl_req = f" curl: {curlify.to_curl(request.request)}"

    response_info = '==================RESPONSE=INFO=================='
    response_time = f"Response time sec: {request.elapsed.total_seconds()}"
    response_code = f"Response code: {request.status_code}"

    formatted_info = f'{title}\n{request_method}\n{request_url}\n{request_head}\n\n{curl}\n{curl_req}' \
                     f'\n\n{response_info}\n{response_time}\n{response_code}'

    allure.attach(formatted_info, attachment_type=AttachmentType.TEXT, name='Параметры запроса и ответа')
