import re
from assertpy import assert_that

class RegexAsserts:

    @staticmethod
    def assert_contains_word(expected_word, string, expected=True):
        # Проверяет содержится конкретное слово в тексте
        pattern = re.compile(fr"\b{re.escape(expected_word)}\b", re.IGNORECASE)
        match = pattern.search(string)

        if expected:
            assert_that(match, description=f"Текст '{string}' не содержит слово'{expected_word}'").is_true()

        elif not expected:
            assert_that(match, description=f"Текст '{string}' содержит слово'{expected_word}'").is_false()

    @staticmethod
    def assert_contains_number(value, expected=True):
        # Проверяет содержатся ли цифры
        if expected:
            assert re.search(r"\d", str(value)), f"Текст '{value}' не содержит числа"

        elif not expected:
            assert not re.search(r"\d", str(value)), f"Текст '{value}' содержит числа хотя не должен"

    @staticmethod
    def assert_contains_letters(text, expected=True):
        # Проверяет содержатся ли буквы по сути проверка на не пустое поле
        pattern = r'\b\w+\b'

        if expected:
            assert re.search(pattern, text), f"Текст '{text}' не содержит букв"
        else:
            assert not re.search(pattern, text), f"Текст '{text}' содержит букв"

    @staticmethod
    def assert_contains_mail(text):
        # проверка на валидацию email
        pattern = r"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$"

        assert re.search(pattern, text), f"'{text} не валидный mail адресс"

    @staticmethod
    def assert_contains_phone_10_digits_format(text):
        pattern = r"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$"
        #7076422361 проверяет номера в таком формате
        assert re.search(pattern, text), f"'{text} не валидный номер телефона"


    @staticmethod
    def assert_contains_english_words(text):
        #проверяет только на содержание слов на латинице
        pattern = r"\b[a-zA-Z]+\b"
        assert re.search(pattern, text), f"'{text} содежит слова неналатинице"

    @staticmethod
    def assert_contains_words_(text):
        # проверка на содержание и цифры и букв (должна быть и цифра и буквы)
        pattern = r"\b(?=\w+\s*\d|\d+\s*\w)\w+\s*\d?\w*\b"
        assert re.search(pattern, text), f"'{text} несодержит слово либо цифру"






