import json
class FileManager:
    def __init__(self, file):
        self.file = file

    def get_file_for_upload(self):
        try:
            with open(self.file, "rb") as file:
                files = {"file": file.encode('base64').decode('utf-8')}
                return files
        except IOError:
            print(f"Error uploading file: {self.file}")

a = FileManager('/Users/muratisaev/PycharmProjects/apistt/.env')