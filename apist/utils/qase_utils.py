import curlify
from qaseio.pytest import qase


def qase_request_parameters_text(request):
    title = '==================REQUEST=INFO=================='
    request_method = f"Request method: {request.request.method}"
    request_url = f"Request URL: {request.url}"
    request_head = f'Request head: {request.request.headers}'
    curl = '==================REQUEST=CURL=================='
    curl_req = f" curl: {curlify.to_curl(request.request)}"

    response_info = '==================RESPONSE=INFO=================='
    response_time = f"Response time sec: {request.elapsed.total_seconds()}"
    response_code = f"Response code: {request.status_code}"
    response_body = f"Response body: {request.content}"

    formatted_info = f'{title}\n{request_method}\n{request_url}\n{request_head}\n\n{curl}\n{curl_req}' \
                     f'\n\n{response_info}\n{response_time}\n{response_code}\n{response_body}'

    file_name = "metadata.txt"

    with open(file_name, "w") as file:
        file.write(formatted_info)

    import os

    file_path = os.path.abspath(file_name)
    qase.attach(file_path, "text/plain")
