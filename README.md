# Apist

Apist - это набор утилит и оберток разработный для простой реализации API тестов.
Этот набор утилит позволит вам легко поддерживать и запускать ваши существующий набор тестов а так же писать новые
тесты быстрее.

Проверить версию Apist можно с командрй:
```
pip show my_package
```

Здесь описаны некоторые функции которые помогут писать тесты быстрее

## Обзор

## HttpManager

Apist позволяет использовать простой API, похожий на requests, для выполнения HTTP-запросов.

`HttpManager` имеет следующий метод:
```python

from apist.utils.http_manager import HttpManager as http

http.request(method='GET', base_url='http://example.kz', raw_path='/main-page', *kwargs)
```


## Assertions

Apiritif предоставляет множество полезных assert-ов, которые могут быть использованы для проверки ответов.

Вот список assert-о, которые могут быть использованы:
```python
response = http.request(method='GET', base_url='http://example.kz', raw_path='/main-page', *kwargs)

# проверка на то что запрос успешный(status code  2xx или 3xx)
response.assert_ok_status_code()

# проверка на то что запрос неуспешный
response.assert_failed_status_code()

# проверка двух значений на соответсвие или не соответсвие через параметр equal
response.assert_equal(equal=False)

# проверка на тип обьект
response.assert_type()

# Проверка на больше 
response.assert_greater()

# Проверка на то что кол-во символов в ответе больше чем
response.assert_response_symbol_sym_more_than()

# Сопоставление json с ответа и передаваемого на соотвествие 
response.assert_response_body_json()

# Проверка на время ответа с сервера не больше чем
response.assert_response_time_not_more_than()

# Soft проверка на наличие какой-либо string в теле ответа
response.assert_contains()


```

Имейте в виду что assert-ы могут дополняться и изменяться
```python

response = http.get("http://example.com/")
response.assert_ok().assert_in_body("Example")
```

## Allure utils
allure_utils содержит различные утилиты для привязки ответов, параметров и т.д. к артефактам отчета

```python
from apist.utils.allure_utils import allure_response_body, allure_response_body_not_json, allure_request_body, 
allure_request_parameters_text

response = http.request(method='GET', base_url='http://example.kz', raw_path='/main-page', *kwargs)

# Привязать тело запроса к отчетам
allure_request_body(res) 

# Привязать тело ответа к отчетам (json)
allure_response_body(res) 

# Привязать тело ответа к отчетам не json
allure_response_body_not_json(res) 

#привязать параметры запроса (header, response time, method, url) в текстовом формате к отчетам
allure_request_parameters_text(res)

```

## Результаты тестов

Для того что бы получить отчет по запуску тестов используем flag --allure Директория. Там будут сгенерированы  
результаты тестов 

### Contants

Список констант и их предназачение
* `RESPONSE_TIME_LIMIT` - лимит по тому за какое время должен прийти ответ с сервера по умолчанию - 15 сек.
